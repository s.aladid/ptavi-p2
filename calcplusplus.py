#!/usr/bin/python3
# -*- coding: utf-8 -*-


import sys
import csv

class Calculadora:

  def suma(self, operando1, operando2):
    return operando1 + operando2
  def resta(self, operando1, operando2):
    return operando1 - operando2

class CalculadoraHija(Calculadora):
  def multi(self, operando1, operando2):
    return operando1 * operando2
  def div(self, operando1, operando2):
    return operando1 / operando2

micalc = CalculadoraHija()

if __name__ == "__main__":
  try:
    fichero = sys.argv[1]

  except ValueError:
    sys.exit("Error: No hay un fichero")

  with open(fichero) as fp:

    entrada = csv.reader(fp)
    for lista in entrada:

      if lista[0] == 'suma':
          print(eval('+'.join(lista[1::])))

      elif lista[0] == 'resta':
          print(eval('-'.join(lista[1::])))

      elif lista[0] == 'multiplica':
          print(eval('*'.join(lista[1::])))

      elif lista[0] == 'divide':
          for i in lista[1::]:
             if int(i) == 0:
                print('no puedes dividir entre cero')
                exit()
          print(eval('/'.join(lista[1::])))
      else:
          print('Introduce una orden válida: suma, resta, multiplica o divide.')
